package ru.aev.threads;

public class HelloRunnable implements Runnable {

    public void run() {
        System.out.println("Текущий поток- " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        for (int i = 10; i > 0; i--) {
            (new Thread(new HelloRunnable())).start();
        }
    }
}